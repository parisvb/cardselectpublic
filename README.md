#ltd.appstart.cardselect readme

##To build for release:

`cd android && ./gradlew assembleRelease && cd ..`

Your APK will get generated at: android/app/build/outputs/apk/app-release.apk

##Build error fixes:

`cd android && ./gradlew clean && cd ..`

delete `android/.gradle` folder

`npm i -D jetifier ; npx jetify`

Run following for clean and bundle:

`react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/build/intermediates/res/merged/release/ && rm -rf android/app/src/main/res/drawable-* && rm -rf android/app/src/main/res/raw/* && cd android && ./gradlew assembleRelease && cd ..`

