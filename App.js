import * as React from 'react';

//nav
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//theme
import { Provider as PaperProvider } from 'react-native-paper';
import { theme } from './src/theme';

//pages
import Home from './src/screens/Home';
import More from './src/screens/More';
import Export from './src/screens/Export';
import Import from './src/screens/Import';
import CreateCard from './src/screens/CreateCard';
import CreateCard2 from './src/screens/CreateCard/CreateCard2';
import CreateCard3 from './src/screens/CreateCard/CreateCard3';
import AddAnother from './src/screens/CreateCard/AddAnother';
import EditDeck from './src/screens/EditDeck';
import ViewDeck from './src/screens/ViewDeck';
import History from './src/screens/History';
import Cards from './src/screens/Cards';
import About from './src/screens/About';

const Stack = createStackNavigator();

export default App = () => {
  // //disable all logging
  // console.log = function () { };
  // console.warn = function () { };
  // console.error = function () { };

  return (
    <PaperProvider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator headerMode={'none'}>
          <Stack.Screen name="Home" component={Home} />         
          <Stack.Screen name="More" component={More} />
          <Stack.Screen name="Export" component={Export} />
          <Stack.Screen name="Import" component={Import} />       
          <Stack.Screen name="CreateCard" component={CreateCard} />
          <Stack.Screen name="CreateCard2" component={CreateCard2} />
          <Stack.Screen name="CreateCard3" component={CreateCard3} />
          <Stack.Screen name="AddAnother" component={AddAnother} />
          <Stack.Screen name="EditDeck" component={EditDeck} />
          <Stack.Screen name="ViewDeck" component={ViewDeck} />
          <Stack.Screen name="History" component={History} />
          <Stack.Screen name="Cards" component={Cards} />
          <Stack.Screen name="About" component={About} />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
}
