import Realm from 'realm';

//schema defs
/////////////////////////////////////////////////////////////////////////////////

const ImportStatusSchema = {
  name: 'ImportStatus',
  primaryKey: 'name',
  properties: {
    name: 'string',
  }
};

const CardSchema = {
  name: 'Card',
  primaryKey: 'uuid',
  properties: {
    pic: 'string',
    audio: 'string',
    text: 'string',
    uuid: 'int'
  }
};

const CurrentDeckSchema = {
  name: 'CurrentDeck',
  primaryKey: 'name',
  properties: {
    name: 'string',
    items: {
      type: 'Card[]'
    }
  }
};

const HistorySchema = {
  name: 'History',
  primaryKey: 'uuid',
  properties: {
    uuid: 'int',
    items: {
      type: 'Card[]'
    }
  }
};

//import assets
/////////////////////////////////////////////////////////////////////////////////

export const hasImported = () => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [ImportStatusSchema] })
      .then(realm => {
        let ImportStatus = realm.objects('ImportStatus');

        var hasImported = false;
        if (ImportStatus.length > 0) {
          hasImported = true;
        }

        //close db
        realm.close();

        resolve(hasImported, realm);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const setImported = () => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [ImportStatusSchema] })
      .then(realm => {
        //add item
        realm.write(() => {
          const item = realm.create('ImportStatus', {
            name: 'Import done'
          });
        });

        //close db
        realm.close();

        resolve('import complete');
      })
      .catch((error) => {
        reject(error);
      });
  });
};

//current deck
/////////////////////////////////////////////////////////////////////////////////

export const setCurrentDeck = (items) => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CurrentDeckSchema, CardSchema] })
      .then(realm => {
        realm.write(() => {
          //normalise
          var cards = [];
          for (var i = 0; i < items.length; i++) {
            cards.push(realm.objectForPrimaryKey('Card', items[i].uuid));
          }

          //first delete
          if (realm.objectForPrimaryKey('CurrentDeck', 'CurrentDeck')) {
            realm.delete(realm.objectForPrimaryKey('CurrentDeck', 'CurrentDeck'));
          }
          //then set
          realm.create('CurrentDeck', {
            name: 'CurrentDeck',
            items: cards
          });
        });

        //close db
        realm.close();

        resolve('current deck set');
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const getCurrentDeck = () => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CurrentDeckSchema, CardSchema] })
      .then(realm => {
        let CurrentDeck = realm.objects('CurrentDeck');

        var returnedCards = [];
        if (CurrentDeck.length > 0) {
          for (var i = 0; i < CurrentDeck[0].items.length; i++) {
            returnedCards.push({
              pic: CurrentDeck[0].items[i].pic,
              audio: CurrentDeck[0].items[i].audio,
              text: CurrentDeck[0].items[i].text,
              uuid: CurrentDeck[0].items[i].uuid
            });
          }
        }

        //close db
        realm.close();

        resolve(returnedCards, realm);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const saveHistoryItemToCurrentDeck = (uuid) => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CurrentDeckSchema, CardSchema, HistorySchema] })
      .then(realm => {
        realm.write(() => {
          //get history item
          let HistoryItem = realm.objectForPrimaryKey('History', uuid);

          //normalise
          var cards = [];
          for (var i = 0; i < HistoryItem.items.length; i++) {
            cards.push(realm.objectForPrimaryKey('Card', HistoryItem.items[i].uuid));
          }

          //first delete
          realm.delete(realm.objectForPrimaryKey('CurrentDeck', 'CurrentDeck'));

          //set
          realm.create('CurrentDeck', {
            name: 'CurrentDeck',
            items: cards
          });
        });

        //close db
        realm.close();

        resolve('history item set to current deck');
      })
      .catch((error) => {
        reject(error);
      });
  });
}

//cards
/////////////////////////////////////////////////////////////////////////////////

export const addCard = (pic, text, audio) => {
  return new Promise((resolve, reject) => {
    var uuid = Date.now();

    Realm.open({ schema: [CardSchema] })
      .then(realm => {
        //add item
        realm.write(() => {
          const item = realm.create('Card', {
            pic: pic,
            audio: audio,
            text: text,
            uuid: uuid
          });
        });

        //close db
        realm.close();

        resolve('item added');
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const importCard = (pic, text, audio, uuid) => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CardSchema] })
      .then(realm => {
        //add item
        realm.write(() => {
          const item = realm.create('Card', {
            pic: pic,
            audio: audio,
            text: text,
            uuid: uuid
          });
        });

        //close db
        realm.close();

        resolve('item added');
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const deleteCard = (uuid) => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CardSchema] })
      .then(realm => {
        //delete item
        realm.write(() => {
          realm.delete(realm.objectForPrimaryKey('Card', uuid));
        });

        //close db
        realm.close();

        deleteEmptyHistoryItems().then(
          () => {
            resolve('item deleted');
          },
          (err) => {
            reject(err);
          });
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const cardList = (skip, length) => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CardSchema, CurrentDeckSchema] })
      .then(realm => {
        //get card ids in current deck
        let CurrentDeck = realm.objects('CurrentDeck');
        var CurrentDeckIDs = [];
        if (CurrentDeck && CurrentDeck.length > 0) {
          for (var i = 0; i < CurrentDeck[0].items.length; i++) {
            CurrentDeckIDs.push(CurrentDeck[0].items[i].uuid);
          }
        }

        //get all cards within range
        var returnedCards = [];
        let cards = realm.objects('Card');

        var count = 0;
        for (var i = 0; i < cards.length; i++) {
          if (count >= skip && count < (skip + length)) {
            returnedCards.push({
              pic: cards[i].pic,
              audio: cards[i].audio,
              text: cards[i].text,
              uuid: cards[i].uuid,
              isSelected: CurrentDeckIDs.indexOf(cards[i].uuid) > -1 ? true : false
            });
          }

          count = count + 1;
        }
        //close db
        realm.close();

        resolve(returnedCards, realm);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const cardExists = (uuid) => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CardSchema] })
      .then(realm => {

        var cardExists = false;
        if (realm.objectForPrimaryKey('Card', uuid)) {
          cardExists = true;
        }     

        //close db
        realm.close();

        resolve(cardExists, realm);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

//history
/////////////////////////////////////////////////////////////////////////////////

export const historyList = (skip, length) => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CardSchema, HistorySchema] })
      .then(realm => {
        //get all cards within range
        var returnedHistory = [];
        let history = realm.objects('History').sorted('uuid', true);
        var count = 0;
        for (let historyItem of history) {
          if (count >= skip && count < (skip + length)) {
            var historyItems = [];
            for (var i = 0; i < historyItem.items.length; i++) {
              historyItems.push({
                pic: historyItem.items[i].pic,
                audio: historyItem.items[i].audio,
                text: historyItem.items[i].text,
                uuid: historyItem.items[i].uuid
              });
            }

            returnedHistory.push({
              uuid: historyItem.uuid,
              items: historyItems
            });
          }

          count = count + 1;
        }
        //close db
        realm.close();

        resolve(returnedHistory, realm);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const latestHistoryItem = () => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CardSchema, HistorySchema] })
      .then(realm => {
        //get all cards within range
        var item = null;
        let history = realm.objects('History').sorted('uuid', true).slice(0,1);
        for (let historyItem of history) {
          if (historyItem.items.length > 0) {
            var historyItems = [];
            for (var i = 0; i < historyItem.items.length; i++) {
              historyItems.push({
                pic: historyItem.items[i].pic,
                audio: historyItem.items[i].audio,
                text: historyItem.items[i].text,
                uuid: historyItem.items[i].uuid
              });
            }

            item = {
              uuid: historyItem.uuid,
              items: historyItems
            };
          }
        }
        //close db
        realm.close();

        resolve(item, realm);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export const saveCurrentDeckToHistory = () => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CurrentDeckSchema, CardSchema, HistorySchema] })
      .then(realm => {
        realm.write(() => {
          //get current deck
          let CurrentDeck = realm.objectForPrimaryKey('CurrentDeck', 'CurrentDeck');

          //normalise
          var cards = [];
          var thumbprint = '';
          for (var i = 0; i < CurrentDeck.items.length; i++) {
            cards.push(realm.objectForPrimaryKey('Card', CurrentDeck.items[i].uuid));
            thumbprint = thumbprint + CurrentDeck.items[i].uuid;
          }

          //get current history
          let history = realm.objects('History').sorted('uuid', true);

          //check if history should be added to
          var shouldSetHistory = true;
          if (cards.length === 0) {
            //empty - don't add to history
            shouldSetHistory = false;
          }
          if (shouldSetHistory && history.length > 0) {
            //check prev history item is not this
            var thumbprintNew = '';
            for (var i = 0; i < history[0].items.length; i++) {
              thumbprintNew = thumbprintNew + history[0].items[i].uuid;
            }

            if (thumbprintNew === thumbprint) {
              //same as last history item - don't add to history
              shouldSetHistory = false;
            }
          }

          if (shouldSetHistory) {
            //add to history
            var uuid = Date.now();
            realm.create('History', {
              uuid: uuid,
              items: cards
            });
          }

          if (history.length > 64) {
            //remove oldest history item
            realm.delete(history[(history.length - 1)]);
          }
        });

        //close db
        realm.close();

        //resolve promise
        resolve();
      })
      .catch((error) => {
        reject(error);
      });
  });
}

export const deleteEmptyHistoryItems = () => {
  return new Promise((resolve, reject) => {
    Realm.open({ schema: [CardSchema, HistorySchema] })
      .then(realm => {
        realm.write(() => {
          //get current history
          let history = realm.objects('History').sorted('uuid', true);

          //find empty histories
          let emptyHistoryUuids = [];
          for (var i = 0; i < history.length; i++) {
            if (history[i].items.length === 0){
              emptyHistoryUuids.push(history[i].uuid);
            }
          }

          //delete empty histories
          for (var i = 0; i < emptyHistoryUuids.length; i++) {
            realm.delete(realm.objectForPrimaryKey('History', emptyHistoryUuids[i]));
          }
        });

        //close db
        realm.close();

        //resolve promise
        resolve();
      })
      .catch((error) => {
        reject(error);
      });
  });
}