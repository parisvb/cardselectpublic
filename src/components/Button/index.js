import React from 'react';
import { View } from 'react-native';
import { Button as RNPButton, Text } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';

export default Button = ({ title, onPress, icon, color }) => {
  return (
    <RNPButton
      mode="contained"
      onPress={onPress}
      color={color}
      style={{ fontSize: 36, borderRadius: 8 }}
    >
      <Icon name={icon} size={24} color="#fff" />
      <View style={{ width: 16, height: 1 }} />
      <Text style={{ fontSize: 18, color: "#fff" }}>{title}</Text>
    </RNPButton>
  );
};