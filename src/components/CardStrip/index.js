import React, { useState } from 'react';
import { View } from 'react-native';
import { Surface, Text, Button } from 'react-native-paper';
import Sound from 'react-native-sound';

import Card from '../Card';

export default CardStrip = ({ items, onDelete }) => {
  const onPlay = () => {
    // Enable playback in silence mode
    Sound.setCategory('Playback');

    //play all sounds on list
    var soundList = [...items];
    function playSound() {
      if (soundList.length > 0) {
        var sound = new Sound(soundList.shift().audio, Sound.MAIN_BUNDLE, (error) => {
          if (error) {
            return;
          }
          // Play the sound with an onEnd callback
          sound.play((success) => {
            if (success) {
              setTimeout(() => {
                //call again
                playSound();
              }, 10)
            }
          });
        });
      }
    }

    //kick off recursive process
    playSound();
  }


  return (
    <View style={{
      //aspectRatio: 1.9,
      paddingTop: 3,
      paddingBottom: 8,
      backgroundColor: '#FFCC00'
    }}>
      <View style={{

        width: '100%',
        height: 15,
        backgroundColor: '#FFCC00'
      }}></View>

      <View style={{
        //flex: 1,
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e2b600'

      }}>
        <Card
          onClick={() => { }}
          width="25%"
        />
        {items.map((item, index) => (
          <Card
            item={item}
            onClick={() => { }}
            key={index}
            width="25%"
          />
        ))}
        <Card
          onClick={() => { }}
          width="25%"
        />

      </View>
      <View style={{
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        height: 40,
        backgroundColor: '#FFCC00',
        paddingTop: 5
      }}>
        <Button
          onPress={() => { onDelete() }}
        >Delete</Button>
        <Button
          onPress={onPlay}
        >Play</Button>
      </View>

    </View>
  );
};
