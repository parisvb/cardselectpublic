import React from 'react';
import { View, Text, TouchableOpacity, Button } from 'react-native';
import Card from '../Card';
//import { Button } from 'react-native-paper';

export default HistoryItem = ({ items, uuid, onClick }) => {
  return (
    <>
      <View style={{
        paddingTop: 3,
        paddingBottom: 6,
        backgroundColor: '#FFCC00',
        marginBottom: 10
      }}>
        <View style={{
          flexDirection: 'row-reverse',
          padding: 5,
        }}>
          <Button
            title={'Select'}
            onPress={() => {onClick(uuid)}}
          />
        </View>

        <View style={{
          flexDirection: 'row',
          width: '100%'
        }}>
          <Card
            item={items[0]}
            onClick={() => { }}
            animationDisabled={true}
            width="25%"
          />
          <Card
            item={items[1]}
            onClick={() => { }}
            animationDisabled={true}
            width="25%"
          />
          <Card
            item={items[2]}
            onClick={() => { }}
            animationDisabled={true}
            width="25%"
          />
          <Card
            item={items[3]}
            onClick={() => { }}
            animationDisabled={true}
            width="25%"
          />
        </View>

        <View style={{
          flexDirection: 'row',
        }}>
          <Card
            item={items[4]}
            onClick={() => { }}
            animationDisabled={true}
            width="25%"
          />
          <Card
            item={items[5]}
            onClick={() => { }}
            animationDisabled={true}
            width="25%"
          />
          <Card
            item={items[6]}
            onClick={() => { }}
            animationDisabled={true}
            width="25%"
          />
          <Card
            item={items[7]}
            onClick={() => { }}
            animationDisabled={true}
            width="25%"
          />
        </View>
      </View>
    </>
  );
};
