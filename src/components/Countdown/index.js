import React, { useState, useEffect } from 'react';
import * as Progress from 'react-native-progress';

const CountdownProgBarChild = ({ timestampStart, duration, finished }) => {
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    const timestamp = Date.now();
    var calcProgress = (timestamp - timestampStart) / duration;
    if (calcProgress <= 1) {
      var timerID = setInterval(() => {
        setProgress(calcProgress);
      }, 200);

      //console.warn(calcProgress);

      return () => {
        clearInterval(timerID);
      };
    }
    else {
      //end
      if (progress < 1){
        setProgress(1);
        finished();
      }
    }
  }, [progress]);

  return (
    <Progress.Bar progress={progress} width={null} height={16} useNativeDriver={true} />
  );
};

export const ProgBar = ({ progress }) => {
  return (
    <Progress.Bar progress={progress} width={null} height={16} useNativeDriver={true} />
  );
};

export const CountdownProgBar = ({ duration, finished }) => {
  const timestamp = Date.now();

  return (
    <CountdownProgBarChild timestampStart={timestamp} duration={duration} finished={finished} />
  );

};