import * as React from 'react';
import { Text } from 'react-native';

export const Heading = ({ children }) => {
  return (
    <Text style={{marginBottom:5, fontSize:20, color: '#aaa'}}>
      {children}
    </Text>
  );
}

export const SubHeading = ({ children }) => {
  return (
    <Text style={{marginBottom:5, fontSize:14, color: '#666'}}>
      {children}
    </Text>
  );
}