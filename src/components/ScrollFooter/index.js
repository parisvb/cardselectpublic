import React from 'react';
import { ProgressBar } from 'react-native-paper';

import {
  StyleSheet,
  View
} from 'react-native';

export default ScrollFooter = ({ atEnd }) => {
  return (
    <View style={styles.footer}>
      {(!atEnd) && (<ProgressBar indeterminate style={{ margin: 20 }} />)}
    </View>

  );
};

const styles = StyleSheet.create({
  footer: {
    height: 80
  }
});
