import React from 'react';
import { Button, Paragraph, Dialog } from 'react-native-paper';

export const DialogYesNo = ({ visible, text, yesCallback, noCallback }) => {
  const yes = () => {
    yesCallback();
  }

  const no = () => {
    noCallback();
  }

  return (
    <Dialog visible={visible} onDismiss={no}>
      <Dialog.Title>Alert</Dialog.Title>
      <Dialog.Content>
        <Paragraph>{text}</Paragraph>
      </Dialog.Content>
      <Dialog.Actions>
        <Button onPress={no} style={{marginRight: 30}}>NO</Button><Button onPress={yes}>Yes</Button>
      </Dialog.Actions>
    </Dialog>
  );
};

export const DialogPermissions = ({ visible, setPermissionsCallback, cancelCallback }) => {
  const setPermissions = () => {
    setPermissionsCallback();
  }

  const cancel = () => {
    cancelCallback();
  }  

  return (
    <Dialog visible={visible} onDismiss={cancel}>
      <Dialog.Title>Permissions error</Dialog.Title>
      <Dialog.Content>
        <Paragraph>Card select has not got permission to perform the requested action.</Paragraph>
        <Paragraph>Please set permissions by clicking the button below.</Paragraph>
      </Dialog.Content>
      <Dialog.Actions>
        <Button onPress={setPermissions} style={{marginRight: 30}}>Set permissions</Button>
        <Button onPress={cancel} style={{marginRight: 30}}>Cancel</Button>
      </Dialog.Actions>
    </Dialog>
  );
};