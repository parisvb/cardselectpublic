import React, { useState } from 'react';
import { View } from 'react-native';
import { Surface, Text } from 'react-native-paper';

import Card from '../Card';

export default SelectGrid = ({ items, clearItem }) => {
  return (
    <View style={{
      //aspectRatio: 1.7,
      paddingTop: 3,
      paddingBottom: 8,
      backgroundColor: '#FFCC00'
    }}>
      <View style={{
        //flex: 1,
        flexDirection: 'row',
        //width: '100%'
      }}>
        <Card
          item={items[0]}
          onClick={() => { }}
          onClose={clearItem}
          width="25%"
        />
        <Card
          item={items[1]}
          onClick={() => { }}
          onClose={clearItem}
          width="25%"
        />
        <Card
          item={items[2]}
          onClick={() => { }}
          onClose={clearItem}
          width="25%"
        />
        <Card
          item={items[3]}
          onClick={() => { }}
          onClose={clearItem}
          width="25%"
        />
      </View>

      <View style={{
        //flex: 1,
        flexDirection: 'row',
      }}>
        <Card
          item={items[4]}
          onClick={() => { }}
          onClose={clearItem}
          width="25%"
        />
        <Card
          item={items[5]}
          onClick={() => { }}
          onClose={clearItem}
          width="25%"
        />
        <Card
          item={items[6]}
          onClick={() => { }}
          onClose={clearItem}
          width="25%"
        />
        <Card
          item={items[7]}
          onClick={() => { }}
          onClose={clearItem}
          width="25%"
        />
      </View>
    </View>
  );
};
