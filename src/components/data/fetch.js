import React, { useState } from 'react';
import { historyList, cardList } from '../../../database/realm';

export const HistoryList = ({ onLoaded, updateFlag, skip, length }) => {
  if (updateFlag) {
    setTimeout(() => {
      historyList(skip, length).then(
        (data) => {
          //view
          onLoaded(data);
        },
        (err) => {
          //TODO - handle error
        },
      );
    }, 100);
  }

  return <></>;
};

export const CardList = ({ onLoaded, updateFlag, skip, length }) => {
  if (updateFlag) {
    setTimeout(() => {
      cardList(skip, length).then(
        (data) => {
          //view
          onLoaded(data);
        },
        (err) => {
          //TODO - handle error
        },
      );
    }, 100);
  }

  return <></>;
};
