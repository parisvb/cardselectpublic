import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import FlexImage from 'react-native-flex-image';
import { Surface, Text } from 'react-native-paper';
import Button from 'react-native-button';
//import { View as AnimatedView } from 'moti'

export default Card = ({ item, onClick, onClose, isSelected, width, animationDisabled }) => {
  const onClickHandler = () => {
    if (!isSelected && onClick) {
      onClick(item);
    }
  }

  const onCloseHandler = () => {
    //initiate close
    onClose(item);
  }

  return (
    <>
      {(item) && (<>
        <TouchableOpacity style={{
          width: width,
          position: 'relative',

        }}
          onPress={onClickHandler}
        >
          {/* <AnimatedView
            from={{
              opacity: animationDisabled ? 1 : 0,
              scale: animationDisabled ? 1 : 0.5,
            }}
            animate={{
              opacity: 1,
              scale: 1,
            }}
            transition={{
              type: 'timing',
            }}
          > */}
            <Surface style={{
              alignItems: 'center',
              justifyContent: 'center',
              padding: 5,
              elevation: 4,
              margin: 5,
              borderRadius: 4,
              opacity: isSelected ? 0.5 : 1
            }}>
              {(onClose) && (
                <View
                  style={{
                    position: 'absolute',
                    right: -5,
                    top: -5,
                    borderRadius: 20,
                    width: 40,
                    height: 40,
                    zIndex: 10
                  }}
                >
                  <Button
                    title="X"
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      borderRadius: 20,
                      height: 40,
                      width: 40,
                      backgroundColor: 'red',
                      color: 'white',
                      fontSize: 28
                    }}

                    onPress={onCloseHandler}
                  >
                    X</Button>
                </View>
              )}
              <FlexImage
                source={{ uri: item.pic }}
                style={{
                  width: '100%',
                  borderRadius: 2,
                  overflow: 'hidden'
                }}
              />
              {(item.text !== '') && (
                <Text
                  numberOfLines={1}
                >{item.text}</Text>
              )}
            </Surface>
          {/* </AnimatedView> */}
        </TouchableOpacity>
      </>
      )}

      {(!item) && (
        <View style={{
          width: width,
        }} >
          <View style={{
            alignItems: 'center',
            justifyContent: 'center',
            padding: 5,
            elevation: 0,
            margin: 5,
            borderRadius: 4,
            backgroundColor: '#e2b600'
          }}>

            <FlexImage
              source={require('../../assets/transparent.png')}
              style={{
                width: '100%',
                borderRadius: 2,
                overflow: 'hidden'
              }}
            />
            <Text></Text>
          </View>
        </View>
      )}
    </>
  );
};