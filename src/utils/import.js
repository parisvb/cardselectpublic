import { filePaths, writeFile } from './fs'
import { cardExists, importCard } from '../../database/realm';
import { PermissionsAndroid } from 'react-native';

var successCallback = null;
var progressUpdateCallback = null;
var data = [];
var totalItems = 0;
var doneItems = 0;
var errorItems = 0;
var lastErrror = "";
var duplicateItems = 0;
var permissionChecked = false;

const checkPerm = () => {
  return new Promise((resolve, reject) => {
    if (permissionChecked) {
      resolve();
    }

    PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {
      title: "Select Card permission",
      message:
        "Card select needs to be able to write files to import your cards.",
      buttonNeutral: "Ask Me Later",
      buttonNegative: "Cancel",
      buttonPositive: "OK"
    }
    ).then(() => {
      permissionChecked = true;
      resolve();
    }, (err) => {
      console.warn(err);
      reject();
    })
  });
}

const saveItem = (item) => {
  return new Promise((resolve, reject) => {
    //check if already in db
    cardExists(item.uuid).then((cardExists) => {
      if (!cardExists) {
        //card does not exist, add it
        var picPath =  filePaths.PicturesDirectoryPath + '/' + item.uuid + '.jpg';
        let audioPath = filePaths.MusicDirectoryPath + '/' + item.uuid + '.wav';

        //check perm
        checkPerm().then(() => {
          // write img
          writeFile(picPath, item.pic, 'base64')
            .then((success) => {
              //write audio
              writeFile(audioPath, item.audio, 'base64')
                .then((success) => {
                  //write to db
                  importCard(picPath, item.text, audioPath, item.uuid)
                    .then(() => {
                      //card added
                      doneItems = doneItems + 1;
                      resolve();
                    });

                })
                .catch((err) => {
                  //log and continue
                  lastErrror = err.message;
                  errorItems = errorItems + 1;
                  resolve();
                });
            })
            .catch((err) => {
              //log and continue
              lastErrror = err.message;
              errorItems = errorItems + 1;
              resolve();
            });
        });
      }
      else {
        //card already in db
        duplicateItems = duplicateItems + 1;
        resolve();
      }
    });
  });
}

const finished = () => {
  //call success
  successCallback();
}

const importItem = () => {
  if (data.length > 0) {
    //get item
    var item = data.shift();

    saveItem(item).then(() => {
      progressUpdateCallback({
        done: doneItems,
        duplicate: duplicateItems,
        error: errorItems,
        total: totalItems
      })

      //now goto next
      setTimeout(() => {
        importItem();
      }, 10);
    });
  }
  else {
    finished();
  }
}

export const importData = (importData, updateCallback, finishCallback) => {
  successCallback = finishCallback;
  progressUpdateCallback = updateCallback;
  data = importData;
  totalItems = importData.length;
  doneItems = 0;
  errorItems = 0;
  duplicateItems = 0;

  importItem();
}