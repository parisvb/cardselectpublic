import { cardExists, importCard } from '../../database/realm';
import { filePaths } from './fs'

var successCallback = null;
var progressUpdateCallback = null;
var data = [];
var totalItems = 0;
var doneItems = 0;
var errorItems = 0;
var duplicateItems = 0;

const saveItem = (item) => {
  return new Promise((resolve, reject) => {
    //check if already in db
    cardExists(item.uuid).then((cardExists) => {
      if (!cardExists) {
        //card does not exist, add it (it's already in asset folder so just write to db)
        var picPath = filePaths.assetDirectoryPath + '/data/' + item.pic;
        let audioPath = filePaths.assetDirectoryPath + '/data/' + item.audio;

        //write to db
        importCard(picPath, item.text, audioPath, item.uuid)
          .then(() => {
            //card added
            doneItems = doneItems + 1;
            resolve();
          }, (err) => {
            console.warn(err);
            resolve();
          });
      }
      else {
        //card already in db
        duplicateItems = duplicateItems + 1;
        resolve();
      }
    });
  });
}

const finished = () => {
  //call success
  successCallback();
}

const importItem = () => {
  if (data.length > 0) {
    //get item
    var item = data.shift();

    saveItem(item).then(() => {
      progressUpdateCallback({
        done: doneItems,
        duplicate: duplicateItems,
        error: errorItems,
        total: totalItems
      })

      //now goto next
      setTimeout(() => {
        importItem();
      }, 10);
    });
  }
  else {
    finished();
  }
}

export const importData = (importData, updateCallback, finishCallback) => {
  successCallback = finishCallback;
  progressUpdateCallback = updateCallback;
  data = importData;
  totalItems = importData.length;
  doneItems = 0;
  errorItems = 0;
  duplicateItems = 0;

  importItem();
}