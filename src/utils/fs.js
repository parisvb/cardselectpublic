import RNFS from 'react-native-fs';
import { AudioUtils } from 'react-native-audio';

export const filePaths = {
  assetDirectoryPath: 'asset:',
  PicturesDirectoryPath: 'file://' + RNFS.PicturesDirectoryPath,
  MusicDirectoryPath: AudioUtils.MusicDirectoryPath,
  DocumentDirectoryPath: RNFS.DocumentDirectoryPath
}

export const fileInfo = (path) => {
  return {
    filename: path.split('/')[path.split('/').length - 1]
  }
}

export const readFile = (path, encoding) => {
  return new Promise((resolve, reject) => {
    //check if asset file
    if (path.startsWith(filePaths.assetDirectoryPath)) {
      //console.warn('ASSETS!')

      RNFS.readFileAssets(path.slice(7), encoding)
        .then((result) => {
          resolve(result);
        }, (err) => {
          //return error
          console.warn(err);
          reject(err);
        });
    }
    else {
      //console.warn('READ FILE')
      RNFS.readFile(path, encoding)
        .then((result) => {
          resolve(result);
        }, (err) => {
          //return error
          console.warn(err);
          reject(err);
        });
    }
  });
}

export const writeFile = (path, data, encoding) => {
  return new Promise((resolve, reject) => {
    RNFS.writeFile(path, data, encoding)
      .then(() => {
        resolve();
      })
      .catch((err) => {
        //return error
        console.warn(err);
        reject(err);
      });
  });
}

export const copyFile = (fromPath, toPath) => {
  return new Promise((resolve, reject) => {
    RNFS.copyFile(fromPath, toPath)
      .then(() => {
        resolve(toPath);
      })
      .catch((err) => {
        //return error
        console.warn(err);
        reject(err);
      });
  });
}
