import { readFile, writeFile } from './fs';
import { zip, unzip, unzipAssets, subscribe } from 'react-native-zip-archive';

var exportObj = [];
var path = '';
var successCallback = null;
var progressUpdateCallback = null;
var data = [];
var totalItems = 0;

const saveItem = (item) => {
  return new Promise((resolve, reject) => {
    //get image and convert to base64
    readFile(item.pic, 'base64')
      .then((base64img) => {
        readFile(item.audio, 'base64')
          .then((base64audio) => {
            exportObj.push(
              {
                pic: base64img,
                audio: base64audio,
                text: item.text,
                uuid: item.uuid
              }
            );

            resolve();
          }, (err) => {
            //ignore and continue
            console.warn(err);
            resolve();
          });
      }, (err) => {
        //ignore and continue
        console.warn(err);
        resolve();
      });
  });
}

const finished = () => {
  // write the file
  writeFile(path  + '/CardSelectData.json', JSON.stringify(exportObj), 'utf8')
    .then(() => {
      //zip file
      zip(path + '/CardSelectData.json', path + '/CardSelectData.zip').then(() => {
        exportObj = [];
        successCallback();
      })
    })
    .catch((err) => {
      console.log(err.message);
    });
}

const exportItem = () => {
  if (data.length > 0) {
    //get item
    var item = data.shift();

    saveItem(item).then(() => {
      progressUpdateCallback({
        done: (totalItems - data.length),
        total: totalItems
      })

      //now goto next
      setTimeout(() => {
        exportItem();
      }, 10);
    });
  }
  else {
    finished();
  }
}

export const exportToFile = (fullPath, exportData, updateCallback, finishCallback) => {
  exportObj = [];
  path = fullPath;
  successCallback = finishCallback;
  progressUpdateCallback = updateCallback;
  data = exportData;
  totalItems = exportData.length;

  exportItem();
}