import React, { useState, useEffect } from 'react';
import { Appbar } from 'react-native-paper';
import { Text } from 'react-native';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Container, Center } from '../../components/ui/layout.js';
import { Heading, SubHeading } from '../../components/ui/text.js';
import Button from '../../components/Button';
import { cardList } from '../../../database/realm';
import { filePaths } from '../../utils/fs';
import { exportToFile } from '../../utils/export';

import Share from 'react-native-share';

export default Export = ({ navigation }) => {
  const [progress, setProgress] = useState(0);
  const [progTxt, setProgTxt] = useState('Starting export...');
  const [finished, setFinished] = useState(false);

  //set path
  var path = filePaths.DocumentDirectoryPath;

  useEffect(() => {
    //get current deck from db
    cardList(0, 10000).then((data) => {
      setProgTxt('Exporting ' + data.length + ' cards...');

      //export callbacks
      const update = (progress) => {
        setProgTxt('Exporting ' + progress.done + ' of ' + progress.total + ' cards...');
      }
      const finished = () => {
        setFinished(true);
      }

      //start export process
      exportToFile(path, data, update, finished);

    }, (err) => {
      //TODO - handle error
    });
  }, []);


  const share = () => {
    //now show share
    var shareOptions = {
      title: 'Share cards',
      message: 'You can import this file into another card select app',
      url: 'file://' + path + '/CardSelectData.zip'
    };
    Share.open(shareOptions)
      .then((res) => { console.warn(res) })
      .catch((err) => { err && console.warn(err); });
  }

  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Appbar.Content title="Export cards" />
      </Appbar.Header>

      <Container>
        <Center>
          <GoldenPadder>
            <BigSpacer />
            <Heading>Exporting cards...</Heading>
            {(!finished) && (
              <SubHeading>{progTxt}</SubHeading>
            )}
            {(finished) && (<>
              <SubHeading>Finished card export, please press share button below:</SubHeading>
              <BigSpacer />
              <Button
              title="Share cards"
              icon="share"
              onPress={share}
            />
            </>)}

          </GoldenPadder>
        </Center>
      </Container>
    </>
  );
};
