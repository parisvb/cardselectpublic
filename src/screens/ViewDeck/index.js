import React, { useState, useEffect } from 'react';
import GLOBAL from '../../global';
import { Appbar } from 'react-native-paper';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  StatusBar,
} from 'react-native';

import Sound from 'react-native-sound';

import { getCurrentDeck, saveCurrentDeckToHistory } from '../../../database/realm';
import Card from '../../components/Card';
import CardStrip from '../../components/CardStrip';

export default ViewDeck = ({ navigation }) => {
  const [deckData, setDeckData] = useState([]);

  //for select grid
  const [items, setItems] = useState([]);

  useEffect(() => {
    setTimeout(() => {
      //save current deck to history
      saveCurrentDeckToHistory().then(() => {
        //get current deck from db
        getCurrentDeck().then((data) => {
          //save this deck to global
          GLOBAL.latestHistoryItem = data;

          setDeckData(data);
        }, (err) => {
          //TODO - handle error
        });
      }, (err) => {
        //TODO - handle error
      });
    }, 200)
  }, []);


  const cardClick = (itemToAdd) => {
    //add
    setItems([...items, itemToAdd]);

    //play
    var sound = new Sound(itemToAdd.audio, Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        console.warn('card audio play error', error);

        return;
      }
      // Play the sound
      sound.play((success) => {
        console.warn('sound play', success)

      });
    });
  }

  const onDelete = () => {
    //remove all
    setItems([]);
  }

  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Appbar.Content title="View deck" />
      </Appbar.Header>

      <CardStrip
        items={items}
        onDelete={onDelete}
      />

      <SafeAreaView style={styles.container}>
        <FlatList
          numColumns={4}
          data={deckData}
          renderItem={({ item }) => <Card
            item={item}
            isSelected={item.isSelected}
            onClick={cardClick}
            animationDisabled
            width="25%"
          />}
          keyExtractor={item => item.uuid}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  }
});
