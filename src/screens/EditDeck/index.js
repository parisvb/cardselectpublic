import React, { useState, useEffect } from 'react';
import { Appbar } from 'react-native-paper';
import {
  FlatList,
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  StatusBar,
} from 'react-native';

import Sound from 'react-native-sound';
import Woosh from '../../assets/woosh.wav';

import { setCurrentDeck, getCurrentDeck } from '../../../database/realm';

import { CardList } from '../../components/data/fetch';
import Card from '../../components/Card';
import SelectGrid from '../../components/SelectGrid';
import ScrollFooter from '../../components/ScrollFooter';

var sound = null;

export default EditDeck = ({ navigation }) => {
  const [skip, setSkip] = useState(0);
  const [updateFlag, setUpdateFlag] = useState(true);
  const [deckData, setDeckData] = useState([]);
  const [soundLoadSuccess, setSoundLoadSuccess] = useState(false);
  const [atEnd, setAtEnd] = useState(false);

  //for select grid
  const [items, setItems] = useState([]);

  const loadData = () => {
    setUpdateFlag(true);
  }

  useEffect(() => {
    //get current deck from db
    getCurrentDeck().then((data) => {
      setItems(data);

      //load woosh close sound
      sound = new Sound(Woosh, (error) => {
        if (error) {
          return;
        }

        //set success
        setSoundLoadSuccess(true);
      });

    }, (err) => {
      //TODO - handle error
    });
  }, []);

  const onLoaded = (data) => {
    //update state
    setUpdateFlag(false);
    setSkip(skip + 32);

    //append data
    setDeckData([...deckData, ...data]);

    if (data.length === 0){
      //end of results
      setAtEnd(true);
    }    
  };

  const cardClick = (itemToAdd) => {
    //update deck
    setDeckData(deckData.map(i =>
      i.uuid === itemToAdd.uuid
        ? { ...i, isSelected: true }
        : i
    ));

    //save to db
    setCurrentDeck([...items, itemToAdd]).then(() => {
      //add
      setItems([...items, itemToAdd]);
    }, (err) => {
      //TODO - handle error
    });
  }

  const clearItem = (itemToClear) => {
    //update deck
    setDeckData(deckData.map(i =>
      i.uuid === itemToClear.uuid
        ? { ...i, isSelected: false }
        : i
    ));

    //find and remove
    var filteredItems = items.filter((item) => itemToClear.uuid !== item.uuid);
    //save to realm db
    setCurrentDeck(filteredItems).then(() => {
      //update
      setItems(filteredItems);

      //play close woosh sound
      if (soundLoadSuccess) {
        sound.play((success) => {
        });
      }
    }, (err) => {
      //TODO - handle error
    });
  }

  return (
    <>
      <CardList
        onLoaded={onLoaded}
        updateFlag={updateFlag}
        skip={skip}
        length={32}
      />
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Appbar.Content title="Edit deck" />
        <Appbar.Action
          icon="check"
          onPress={() => {
            navigation.navigate('ViewDeck');
          }} />
      </Appbar.Header>

      <SelectGrid
        items={items}
        clearItem={clearItem}
      />

      <SafeAreaView style={styles.container}>
        <FlatList
          numColumns={4}
          data={deckData}
          renderItem={({ item }) => <Card
            item={item}
            isSelected={item.isSelected}
            onClick={cardClick}
            animationDisabled
            width="25%"
          />}
          keyExtractor={item => item.uuid}
          onEndReached={loadData}
          onEndReachedThreshold={0.3}
          ListFooterComponent={() => <ScrollFooter atEnd={atEnd} />}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
  }
});
