import * as React from 'react';
import { Appbar } from 'react-native-paper';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Container, Center } from '../../components/ui/layout.js';
import { Heading, SubHeading } from '../../components/ui/text.js';
import Button from '../../components/Button';

export default About = ({ navigation }) => {
  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Appbar.Content title="About" />
      </Appbar.Header>

      <Container>
        <Center>
          <GoldenPadder>
            <BigSpacer />
            <Heading>About...</Heading>
            <SubHeading>Card select has been built to give autistic children a voice.</SubHeading>
            <BigSpacer />
                                  

          </GoldenPadder>
        </Center>
      </Container>
    </>
  );
};
