import React, { useState, useEffect } from 'react';
import { Appbar } from 'react-native-paper';
import { Text } from 'react-native';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Container, Center } from '../../components/ui/layout.js';
import { Heading, SubHeading } from '../../components/ui/text.js';
import Button from '../../components/Button';
import { readFile, copyFile, filePaths, fileInfo } from '../../utils/fs';
import DocumentPicker from 'react-native-document-picker';
import { zip, unzip, unzipAssets, subscribe } from 'react-native-zip-archive';

import { importData } from '../../utils/import';

export default Import = ({ navigation }) => {
  const [isError, setIsError] = useState(false);
  const [isFileError, setIsFileError] = useState(false);
  const [started, setStarted] = useState(false);
  const [progTxt, setProgTxt] = useState('Starting import...');
  const [finished, setFinished] = useState(false);

  const importCards = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      //var fileFullPath = res.uri + '/' + res.name;

      setStarted(true);
      setProgTxt('Extracting file... Please Wait...');

      //react-native-zip-archive has a permission error reading from downloads folder, so first we need to copy
      copyFile(res.uri, filePaths.DocumentDirectoryPath + '/' + res.name).then((zipFilePath) => {
        unzip(zipFilePath, filePaths.DocumentDirectoryPath, 'utf8')
          .then((unzipedFilePath) => {
            const unzipedFileFullPath = unzipedFilePath + '/CardSelectData.json';

            //read file
            readFile(unzipedFileFullPath, 'utf8').then((fileContent) => {
              setProgTxt('Reading import file...');

              //import callbacks
              const update = (progress) => {
                setProgTxt('Imported ' + progress.done + ' of ' + progress.total + ' cards. ' + progress.duplicate + ' duplicates, ' + progress.error + ' errors...');
              }
              const finished = () => {
                setFinished(true);
              }

              try {
                //iterate through and insert
                var data = JSON.parse(fileContent);

                setProgTxt('Importing ' + data.length + ' cards...');

                //start import process
                importData(data, update, finished);

              } catch (err) {
                setIsError(true);
                setIsFileError(true);
              }
            }, (err) => {
              setIsError(true);
              console.warn(err);
            });
          }, (err) => {
            setIsError(true);
            console.warn(err);
          });
      }, (err) => {
        setIsError(true);
        console.warn(err);
      });
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        setIsError(true);
      }
    }
  }

  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Appbar.Content title="Import cards" />
      </Appbar.Header>

      <Container>
        <Center>
          <GoldenPadder>
            <BigSpacer />
            <Heading>Import cards...</Heading>
            {(!isError) && (
              <>
                {(!started) && (
                  <>
                    <SubHeading>Select file to import cards:</SubHeading>
                    <BigSpacer />
                    <Button
                      title="select file"
                      icon="save"
                      onPress={importCards}
                    />
                  </>
                )}
                {(started && !finished) && (
                  <SubHeading>{progTxt}</SubHeading>
                )}
                {(finished) && (<>
                  <SubHeading>Finished card import.</SubHeading>
                  <BigSpacer />
                  <Button
                    title="Finished"
                    icon="check"
                    onPress={() => navigation.navigate('Home')}
                  />
                </>)}
              </>
            )}

            {(isError) && (
              <>
                <SubHeading>There has been an error importing the cards.</SubHeading>
                <BigSpacer />

                {(isFileError) && (
                  <>
                    <SubHeading>The file was invalid. Please select a valid file and try again.</SubHeading>
                    <BigSpacer />
                  </>
                )}
              </>
            )}
          </GoldenPadder>
        </Center>
      </Container>
    </>
  );
};
