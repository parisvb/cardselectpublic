import * as React from 'react';
import { Appbar } from 'react-native-paper';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Container, Center } from '../../components/ui/layout.js';
import { Heading } from '../../components/ui/text.js';
import Button from '../../components/Button';

export default More = ({ navigation }) => {
  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Appbar.Content title="More" />
      </Appbar.Header>

      <Container>
        <Center>
          <GoldenPadder>
            <BigSpacer />
            <Heading>More options...</Heading>
            <BigSpacer />
            <Button
              title="Export cards"
              icon="upload"
              color="green"
              onPress={() => navigation.navigate('Export')}
            />    
            <BigSpacer />
            <Button
              title="Import cards"
              icon="download"
              onPress={() => navigation.navigate('Import')}
            />                                    

          </GoldenPadder>
        </Center>
      </Container>
    </>
  );
};
