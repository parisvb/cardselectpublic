import React, { useState, useEffect } from 'react';
import { Appbar } from 'react-native-paper';
import {
  FlatList,
  SafeAreaView,
  Text,
  View,
  StyleSheet,
  StatusBar,
} from 'react-native';
import { CardList } from '../../components/data/fetch';
import Card from '../../components/Card';
import ScrollFooter from '../../components/ScrollFooter';
import { DialogYesNo } from '../../components/Dialog';

import { deleteCard } from '../../../database/realm';

export default Cards = ({ navigation }) => {
  const [skip, setSkip] = useState(0);
  const [updateFlag, setUpdateFlag] = useState(true);
  const [deckData, setDeckData] = useState([]);
  const [atEnd, setAtEnd] = useState(false);
  const [dialogVisible, setDialogVisible] = useState(false);
  const [itemToDelete, setItemToDelete] = useState(null);

  const loadData = () => {
    setUpdateFlag(true);
  }

  const onLoaded = (data) => {
    //update state
    setUpdateFlag(false);
    setSkip(skip + 32);

    //append data
    setDeckData([...deckData, ...data]);

    if (data.length === 0){
      //end of results
      setAtEnd(true);
    }    
  };

  const addCard = () => {
    navigation.navigate('CreateCard')
  }

  const cardDelete = (itemToDelete) => {
    //set item to delete
    setItemToDelete(itemToDelete);

    //show dialog
    setDialogVisible(true);
  }

  const dialogYes = () => {
    //hide dialog
    setDialogVisible(false);

    //delete card from to realm db
    deleteCard(itemToDelete.uuid).then(() => {
      //remove from list
      const filteredDeckData = deckData.filter(item => item.uuid !== itemToDelete.uuid);
      setDeckData(filteredDeckData);
    }, (err) => {
      //TODO - handle error
      console.warn(err);
    });    
  }

  const dialogNo = () => {
    //hide dialog
    setDialogVisible(false);
  }  

  return (
    <>
      <CardList
        onLoaded={onLoaded}
        updateFlag={updateFlag}
        skip={skip}
        length={32}
      />
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Appbar.Content title="Cards" />
        <Appbar.Action
          icon="plus"
          onPress={addCard} />

      </Appbar.Header>

      <SafeAreaView style={styles.container}>
        <FlatList
          numColumns={4}
          horizontal={false}
          data={deckData}
          renderItem={({ item, index }) => <Card
            item={item}
            onClick={null}
            onClose={cardDelete}
            animationDisabled
            width="25%"
          />}
          keyExtractor={item => item.uuid}
          onEndReached={loadData}
          onEndReachedThreshold={0.3}
          ListFooterComponent={() => <ScrollFooter atEnd={atEnd} />}
        />
      </SafeAreaView>
      <DialogYesNo 
        text="Are you sure you wish to delete this card ?"
        visible={dialogVisible}
        yesCallback={dialogYes}
        noCallback={dialogNo}
      />
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
  }
});
