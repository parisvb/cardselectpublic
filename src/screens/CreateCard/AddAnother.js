import React from 'react';
import { Appbar } from 'react-native-paper';
import Button from '../../components/Button';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Center, Container } from '../../components/ui/layout.js';
import { Heading } from '../../components/ui/text.js';

export default AddAnother = ({ navigation }) => {

  const AnotherYes = () => {
    navigation.navigate('CreateCard');
  }

  const AnotherNo = () => {
    navigation.navigate('Home');
  }

  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => { navigation.goBack() }}
        />
        <Appbar.Content
          title="Card created"
        />
      </Appbar.Header>
      <Container>
        <Center>
          <GoldenPadder>
            <BigSpacer />
            <Heading>Add another card?</Heading>
            <BigSpacer />
            <Button
              title="Yes"
              onPress={AnotherYes}
            />
            <BigSpacer />
            <Button
              title="No"
              onPress={AnotherNo}
            />
            <SmallSpacer />
          </GoldenPadder>
        </Center>
      </Container>
    </>
  );
}
