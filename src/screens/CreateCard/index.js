import React, { useState } from 'react';
import { Appbar, TextInput } from 'react-native-paper';
import Button from '../../components/Button';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js'
import { Center } from '../../components/ui/layout.js'
import { Heading } from '../../components/ui/text.js'
import ImagePicker from 'react-native-image-crop-picker';
import { DialogPermissions } from '../../components/Dialog';
import AndroidOpenSettings from 'react-native-android-open-settings'

export default CreateCard = ({ navigation }) => {
  const [dialogVisible, setDialogVisible] = useState(false);

  const dialogSetPermissions = () => {
    //hide dialog
    setDialogVisible(false);

    //open permissions
    // Open app settings menu
    AndroidOpenSettings.appDetailsSettings()
  }

  const dialogCancel = () => {
    //hide dialog
    setDialogVisible(false);
  }

  const imageOptions = {
    width: 400,
    height: 400,
    cropping: true,
    compressImageQuality: 0.8,
    mediaType: 'photo',
    writeTempFile: false,
  };

  const takePicture = () => {
    ImagePicker.openCamera(imageOptions).then((imageInfo) => {
      //navigate to step 2
      navigation.navigate('CreateCard2', { pic: imageInfo.path });
    }, (err) => {
      console.warn(err)
      if (err.toString().toLowerCase().includes('user')) {
        // User cancelled
      } else {
        //fail - show permissions dialog
        setDialogVisible(true);
      }
    });

  }

  const choosePicture = () => {
    ImagePicker.openPicker(imageOptions).then((imageInfo) => {
      //navigate to step 2
      navigation.navigate('CreateCard2', { pic: imageInfo.path });
    }, (err) => {
      console.warn(err)
      if (err.toString().toLowerCase().includes('user')) {
        // User cancelled
      } else {
        //fail - show permissions dialog
        setDialogVisible(true);
      }
    });
  }

  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => { navigation.goBack() }}
        />
        <Appbar.Content
          title="Step 1: Select Picture"
        />
      </Appbar.Header>

      <Center>
        <GoldenPadder>
          <BigSpacer />
          <Button
            title="Take a photo"
            icon="camera"
            onPress={takePicture}
          />
          <BigSpacer />
          <Button
            title="Choose a photo"
            icon="image"
            onPress={choosePicture}
          />
        </GoldenPadder>
      </Center>

      <DialogPermissions
        visible={dialogVisible}
        setPermissionsCallback={dialogSetPermissions}
        cancelCallback={dialogCancel}
      />
    </>
  );
}