import React, { useState } from 'react';
import { Appbar, TextInput } from 'react-native-paper';
import { View } from 'react-native';
import Button from '../../components/Button';
import FlexImage from 'react-native-flex-image';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Center } from '../../components/ui/layout.js';
import { Heading } from '../../components/ui/text.js';
import Card from '../../components/Card';

export default CreateCard2 = ({ route, navigation }) => {
  const [cardText, setCardText] = useState('');

  //get pic location
  const picPath = route.params.pic;

  const AddText = () => {
    //navigate to step 3
    navigation.navigate('CreateCard3', { pic: picPath, text: cardText });
  };

  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => { navigation.goBack() }}
        />
        <Appbar.Content
          title="Step 2: Add text to card"
        />
      </Appbar.Header>

      <Center>
        <GoldenPadder>
          <Center>
            <BigSpacer />
            <GoldenPadder>
              <Center>
                <Card
                  item={{
                    pic: picPath,
                    audio: '',
                    text: '',
                  }}
                  width="80%"
                />
              </Center>
            </GoldenPadder>
          </Center>
          <SmallSpacer />
          <TextInput
            mode='outlined'
            autoFocus={true}
            label='Card text'
            value={cardText}
            onChangeText={setCardText}
          />
          <SmallSpacer />
          <Button
            title="Add text"
            onPress={AddText}
          />
        </GoldenPadder>
      </Center>
    </>
  );
}