import React, { useState } from 'react';
import { Appbar, TextInput } from 'react-native-paper';
import { Text, View } from 'react-native';
import Button from '../../components/Button';
import FlexImage from 'react-native-flex-image';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Center } from '../../components/ui/layout.js';
import { SubHeading } from '../../components/ui/text.js';
import Card from '../../components/Card';
import { CountdownProgBar, ProgBar } from '../../components/Countdown';
import { AudioRecorder } from 'react-native-audio';
import { filePaths } from '../../utils/fs';

import Sound from 'react-native-sound';
import { addCard } from '../../../database/realm';

export default CreateCard3 = ({ route, navigation }) => {
  const [cardAudio, setCardAudio] = useState('');
  const [isRecording, setIsRecording] = useState(false);
  const [isFinishedRecording, setIsFinishedRecording] = useState(false);
  const [hasPermission, setHasPermission] = useState(true);

  //get card details
  const picPath = route.params.pic;
  const cardText = route.params.text;

  const startRecord = () => {
    //record audio
    var uuid = Date.now();
    let audioPath = filePaths.MusicDirectoryPath + '/' + uuid + '.wav';
    let options = {
      SampleRate: 22050,
      Channels: 1,
      AudioQuality: "Low",
      AudioEncoding: "wav",
      MeteringEnabled: true,
    };

    //set path
    setCardAudio(audioPath);

    AudioRecorder.requestAuthorization().then((isAuthorised) => {
      setHasPermission(isAuthorised);

      if (!isAuthorised) {
        return;
      }

      //set path
      setCardAudio(audioPath);

      //start recording
      AudioRecorder.prepareRecordingAtPath(audioPath, options).then(() => {
        AudioRecorder.startRecording().then(() => {
          setIsRecording(true);
        });
      });

      // AudioRecorder.onFinished = (data) => {
      //   // // Android callback comes in the form of a promise instead.
      //   // if (Platform.OS === 'ios') {
      //   //   this._finishRecording(data.status === "OK", data.audioFileURL, data.audioFileSize);
      //   // }
      // };
    });
  }

  const stopRecord = () => {
    AudioRecorder.stopRecording().then(() => {
      //update state
      setIsFinishedRecording(true);
      setIsRecording(false);
    });
  }

  const playRecording = () => {
    var sound = new Sound(cardAudio, Sound.MAIN_BUNDLE, (error) => {
      if (error) {
        return;
      }
      // Play the sound with an onEnd callback
      sound.play((success) => {
      });
    });
  }

  const tryAgain = () => {
    //update state
    setIsFinishedRecording(false);
    setIsRecording(false);
  }

  const isGood = () => {
    //save to realm db
    addCard(picPath, cardText, cardAudio).then(() => {
      //navigate to step add another
      navigation.navigate('AddAnother');
    }, (err) => {
      //TODO - handle error
    });
  }

  return (
    <>
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => { navigation.goBack() }}
        />
        <Appbar.Content
          title="Step 3: Add audio to card"
        />
      </Appbar.Header>

      <Center>
        <GoldenPadder>
          <Center>
            <BigSpacer />
            <GoldenPadder>
              <Center>
                <Card
                  item={{
                    pic: picPath,
                    audio: '',
                    text: cardText,
                  }}
                  height={12}
                />
              </Center>
            </GoldenPadder>
            {(!hasPermission) && (
              <>
                <SmallSpacer />
                <SubHeading>You must enable permission to record audio, please press record and try again.</SubHeading>
                <SmallSpacer />
              </>
            )}
          </Center>
          <BigSpacer />
          {(!isFinishedRecording) && (
            <>
              {(!isRecording) && (
                <>
                  <ProgBar progress={0} />
                  <BigSpacer />
                  <Button
                    title="Record"
                    onPress={startRecord}
                  />
                </>
              )}
              {(isRecording) && (
                <>
                  <CountdownProgBar duration={5000} finished={stopRecord} />
                  <BigSpacer />
                  <Button
                    title="Stop"
                    onPress={stopRecord}
                  />
                </>
              )}
            </>
          )}

          {(isFinishedRecording) && (
            <>
              <Button
                title="Play"
                onPress={playRecording}
              />
              <BigSpacer />
              <Button
                title="Record again"
                onPress={tryAgain}
              />
              <BigSpacer />
              <Button
                title="Finish"
                onPress={isGood}
              />
            </>
          )}
        </GoldenPadder>
      </Center>
    </>
  );
}