import React, { useState, useEffect } from 'react';
import GLOBAL from '../../global';
import { Appbar } from 'react-native-paper';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
} from 'react-native';

import { saveHistoryItemToCurrentDeck } from '../../../database/realm';

import { Container, Center } from '../../components/ui/layout.js';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Heading, SubHeading } from '../../components/ui/text.js';
import Button from '../../components/Button';

import { latestHistoryItem } from '../../../database/realm';
import { HistoryList } from '../../components/data/fetch';
import HistoryItem from '../../components/HistoryItem';
import ScrollFooter from '../../components/ScrollFooter';

export default History = ({ navigation }) => {
  const [skip, setSkip] = useState(0);
  const [isHistortEmpty, setIsHistortEmpty] = useState(false);
  const [updateFlag, setUpdateFlag] = useState(true);
  const [deckData, setDeckData] = useState([]);
  const [atEnd, setAtEnd] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      latestHistoryItem().then((newItem) => {
        //check if latest item is latest in history
        if (GLOBAL.latestHistoryItem !== null && GLOBAL.latestHistoryItem.uuid !== newItem.uuid) {
          console.warn('NEW HISTORY ITEM...')
  
          // const checkDeckData = () => {
          //   console.warn('DECK DATA', deckData);
          //   if (deckData.length === 0){
          //     setTimeout(() => {
          //       checkDeckData();
          //     }, 300);
          //   }
          // }
          // checkDeckData();

          //reset
          GLOBAL.latestHistoryItem = null;
          navigation.replace('History', {});
        }
      }, (err) => {
        //TODO - handle error
      });
    });

    return unsubscribe;
  }, [navigation]);

  const loadData = () => {
    setUpdateFlag(true);
  }

  const onLoaded = (data) => {
    console.warn('ONLOADED....')
    if (data.length === 0 && skip === 0) {
      //there is no data
      setIsHistortEmpty(true);
    }

    //update state
    setUpdateFlag(false);
    setSkip(skip + 4);

    //append data
    setDeckData([...deckData, ...data]);

    if (data.length === 0) {
      //end of results
      setAtEnd(true);
    }
  };

  const historyClick = (uuid) => {
    //save to db
    saveHistoryItemToCurrentDeck(uuid).then(() => {
      navigation.navigate('EditDeck');
    }, (err) => {
      //TODO - handle error
    });
  }

  return (
    <>
      <HistoryList
        onLoaded={onLoaded}
        updateFlag={updateFlag}
        skip={skip}
        length={4}
      />
      <Appbar.Header>
        <Appbar.BackAction
          onPress={() => {
            navigation.goBack();
          }}
        />
        <Appbar.Content title="History" />
      </Appbar.Header>

      <SafeAreaView style={styles.container}>
        {(isHistortEmpty) && (
          <>
            <Center>
              <GoldenPadder>
                <BigSpacer />
                <Heading>History is empty!</Heading>
                <SmallSpacer />
                <SubHeading>Deck history will appear here when you have viewed your first deck.</SubHeading>
                <SmallSpacer />
                <SubHeading>Please click edit deck, set up your deck and click the tick icon in the top right hand corner of the edit deck screen to view the deck.</SubHeading>
                <BigSpacer />
                <Button
                  title="Edit deck"
                  icon="edit"
                  color="green"
                  onPress={() => navigation.navigate('EditDeck')}
                />
              </GoldenPadder>
            </Center>
          </>
        )}
        <FlatList
          numColumns={1}
          data={deckData}
          renderItem={({ item }) => <HistoryItem
            items={item.items}
            uuid={item.uuid}
            onClick={historyClick}
          />}
          keyExtractor={item => item.uuid}
          onEndReached={loadData}
          onEndReachedThreshold={0.3}
          ListFooterComponent={() => <ScrollFooter atEnd={atEnd} />}
        />
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
  }
});
