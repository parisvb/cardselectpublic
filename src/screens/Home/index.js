import React, { useState, useEffect } from 'react';
import { Appbar } from 'react-native-paper';
import { Text } from 'react-native';
import { GoldenPadder, SmallSpacer, BigSpacer } from '../../components/ui/spacing.js';
import { Container, Center } from '../../components/ui/layout.js';
import { Heading, SubHeading } from '../../components/ui/text.js';
import Button from '../../components/Button';
import { readFile, filePaths } from '../../utils/fs';
import { hasImported, setImported } from '../../../database/realm';
import { importData } from '../../utils/assetImport';

export default Home = ({ navigation }) => {
  const [started, setStarted] = useState(false);
  const [progTxt, setProgTxt] = useState('Starting import...');
  const [finished, setFinished] = useState(false);


  useEffect(() => {
    //check if initial import has been done
    hasImported().then((imported) => {
      if (!imported) {
        //import never been run, run it
        //read file
        readFile(filePaths.assetDirectoryPath + '/data/data.json', 'utf8').then((fileContent) => {
          setStarted(true);
          setProgTxt('Reading import file...');

          //iterate through and insert
          var data = JSON.parse(fileContent);

          setProgTxt('Importing ' + data.length + ' cards...');

          //import callbacks
          const update = (progress) => {
            setProgTxt('Imported ' + progress.done + ' of ' + progress.total + ' cards. ' + progress.duplicate + ' duplicates, ' + progress.error + ' errors...');
          }
          const finished = () => {
            //update db that import has happend
            setImported().then(() => {
              setProgTxt('Finished card import.');

              setTimeout(() => {
                setFinished(true);
              }, 500)
            });
          }

          //start import process
          importData(data, update, finished);
        });
      }
    }, (err) => {
      //TODO - handle error
      console.warn(err);
    });
  }, []);


  return (
    <>
      <Appbar.Header dark={true}>
        <Appbar.Content title="Card Select" />
      </Appbar.Header>

      <Container>
        <Center>
          <GoldenPadder>
            <BigSpacer />
            <Heading>Welcome to Card Select.</Heading>

            {(started && !finished) && (
              <SubHeading>{progTxt}</SubHeading>
            )}
            <BigSpacer />
            <Button
              title="Create card"
              icon="plus"
              onPress={() => navigation.navigate('CreateCard')}
            />
            <BigSpacer />
            <Button
              title="Edit deck"
              icon="edit"
              color="green"
              onPress={() => navigation.navigate('EditDeck')}
            />
            <BigSpacer />
            <Button
              title="History"
              icon="history"
              color="#9d5f0f"
              onPress={() => navigation.navigate('History')}
            />
            <BigSpacer />
            <BigSpacer />
            <BigSpacer />
            <Button
              title="Cards"
              icon="stack-overflow"
              color="darkgrey"
              onPress={() => navigation.navigate('Cards')}
            />
            <BigSpacer />
            <Button
              title="About"
              icon="info-circle"
              color="darkgrey"
              onPress={() => navigation.navigate('About')}
            />
            <BigSpacer />
            <Button
              title="More"
              icon="bars"
              color="darkgrey"
              onPress={() => navigation.navigate('More')}
            />

          </GoldenPadder>
        </Center>
      </Container>
    </>
  );
};
